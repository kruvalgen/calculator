import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MultTest extends BaseTest{

    Calculator calculator = new Calculator();

    @DataProvider
    public Object[][] testBoundaryValue() {
        return new Object[][]{
                {Double.MAX_VALUE, 1, Double.MAX_VALUE},
        };
    }


    @DataProvider
    public Object[][] testMultPositive() {
        return new Object[][]{
                {15, 3, 45},
                {1200, 4, 4800},
                {80, 8, 640},
                {4, 10, 40}
        };
    }


    @Test(dataProvider = "testMultPositive")
    public void multPositiveTest(double firstNum, double secondNum, double result) {
        Assert.assertEquals(calculator.mult(firstNum, secondNum), result);
    }


    @Test(dataProvider = "testBoundaryValue")
    public void multBoundaryValue(double firstNum, double secondNum, double result) {
        Assert.assertEquals(calculator.mult(firstNum, secondNum), result);
    }


}
