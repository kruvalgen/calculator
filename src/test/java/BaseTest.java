import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    @BeforeTest
    public void beforeTest() {
        System.out.println("This method will start at the beginning ");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("This method is run after verification");

    }

}
