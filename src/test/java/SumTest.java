import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SumTest extends BaseTest{

    Calculator calculator = new Calculator();


    @DataProvider
    public Object[][] testSumPositive() {
        return new Object[][]{
                {10, 15, 25},
                {-2, 10, 8},
                {115, 1.6, 116.6},
                {8.37, 9.63, 18}
        };
    }


    @DataProvider
    public static Object[][] testBoundaryValue() {
        return new Object[][]{
                {Double.MAX_VALUE, 0, Double.MAX_VALUE},
        };
    }


    @Test(dataProvider = "testSumPositive", testName = "Sum test positive")
    public void sumPositiveTest(double firstNum, double secondNum, double result) {
        Assert.assertEquals(calculator.sum(firstNum, secondNum), result);
    }


    @Test(dataProvider = "testBoundaryValue")
    public void multBoundaryValue(double firstNum, double secondNum, double result) {
        Assert.assertEquals(calculator.sum(firstNum, secondNum), result);
    }



}
