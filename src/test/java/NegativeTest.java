import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class NegativeTest extends BaseTest {

    Calculator calculator = new Calculator();

    @DataProvider
    public Object[][] testDivNegative() {
        return new Object[][]{
                {15, 2, 7.3},
                {1200, 600, 3},
                {80, 4, 21},
                {4, 1, 5}
        };
    }

    @DataProvider
    public Object[][] testSumNegative() {
        return new Object[][]{
                {48, 6, 15},
                {0, 15, 21},
                {9, 0.9, 10},
                {1, -1, 1}
        };
    }

    @DataProvider
    public Object[][] testDifNegative() {
        return new Object[][]{
                {15, 10, 6},
                {1298, 78, 1210},
                {85, 15, 60},
                {4, 8, 4}
        };
    }

    @DataProvider
    public Object[][] testMultNegative() {
        return new Object[][]{
                {15, 2, 7.3},
                {1200, 600, 3},
                {80, 4, 21},
                {4, 1, 5}
        };
    }

    @Test(dataProvider = "testDivNegative")
    public void divNegativeTest(double firstNum, double secondNum, double result) {
        Assert.assertNotEquals(calculator.div(firstNum, secondNum), result);
    }

    @Test(dataProvider = "testSumNegative", testName = "Sum test negative")
    public void sumNegativeTest(double firstNum, double secondNum, double result) {
        Assert.assertNotEquals(calculator.sum(firstNum, secondNum), result);
    }

    @Test(dataProvider = "testDifNegative")
    public void difNegativeTest(double firstNum, double secondNum, double result) {
        Assert.assertNotEquals(calculator.dif(firstNum, secondNum), result);
    }

    @Test(dataProvider = "testMultNegative")
    public void multNegativeTest(double firstNum, double secondNum, double result) {
        Assert.assertNotEquals(calculator.mult(firstNum, secondNum), result);
    }

}
