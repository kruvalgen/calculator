import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DifTest extends BaseTest{
    Calculator calculator = new Calculator();


    @DataProvider
    public Object[][] testDifPositive() {
        return new Object[][]{
                {15, 10, 5},
                {1298, 78, 1220},
                {85, 15, 70},
                {4, 8, -4}
        };
    }


    @DataProvider
    public Object[][] testBoundaryValue() {
        return new Object[][]{
                {Double.MAX_VALUE, 0, Double.MAX_VALUE},
        };
    }

    @Test(dataProvider = "testDifPositive")
    public void difPositiveTest(double firstNum, double secondNum, double result) {
        Assert.assertEquals(calculator.dif(firstNum, secondNum), result);
    }


    @Test(dataProvider = "testBoundaryValue")
    public void multBoundaryValue(double firstNum, double secondNum, double result) {
        Assert.assertEquals(calculator.dif(firstNum, secondNum), result);
    }

}
