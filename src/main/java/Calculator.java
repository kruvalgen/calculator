

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Calculator {
    public static void main(String[] args) throws IOException {



        double firstNum;
        double secondNum;
        String operator;

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter first number : ");
        firstNum = Double.parseDouble(reader.readLine());
        System.out.print("Enter the operator to calculate (+,-,*,/) : ");
        operator = reader.readLine();
        System.out.print("Enter second number : ");
        secondNum = Double.parseDouble(reader.readLine());


        Calculator calculator = new Calculator();
        calculator.calculated(firstNum, secondNum, operator);


    }


    public void calculated(double firstNum, double secondNum, String operator) {

        switch (operator) {
            case "+":
                sum(firstNum, secondNum);
                break;
            case "-":
                dif(firstNum, secondNum);
                break;
            case "*":
                mult(firstNum, secondNum);
                break;
            case "/":
                try {
                    div(firstNum, secondNum);
                } catch (NumberFormatException ex) {
                    System.out.println("Сan not be divided by 0");
                    return;
                }
                break;

        }

    }

    public double sum(double firstNum, double secondNum) {
        double result = firstNum + secondNum;
        System.out.println("Result of summary:  " + result);
        return result;
    }

    public double dif(double firstNum, double secondNum) {
        double result = firstNum - secondNum;
        System.out.println("Result of difference:  " + result);
        return result;
    }

    public double mult(double firstNum, double secondNum) {
        double result = firstNum * secondNum;
        System.out.println("Result of multiplication :  " + result);
        return result;
    }

    public double div(double firstNum, double secondNum) {
        if (secondNum == 0) {
            throw new NumberFormatException("Сan not be divided by 0");
        }
        double result = firstNum / secondNum;
        System.out.println("Division result:  " + result);
        return result;
    }

}



